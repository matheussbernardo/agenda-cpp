include "pessoa.hpp"
#include <string>

using namespace std;
Pessoa::Pessoa() {
		nome = "nulo";
		idade = 0;
		telefone = "nulo";
}
Pessoa::Pessoa(string nome, int idade, string telefone) {
        this->nome  = nome;
        this->idade = idade;
        this->telefone = telefone;
}

int Pessoa::getIdade() {
        return idade;
}

void Pessoa::setIdade(int idade) {
        this->idade = idade;
}

void Pessoa::setNome(string nome) {
		this->nome = nome;
}

string Pessoa::getNome() {
		return nome;
}

void Pessoa::setTelefone(string telefone) {
		this->telefone = telefone;
}

string Pessoa::getTelefone() {
		return telefone;
}
