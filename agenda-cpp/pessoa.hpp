#ifndef PESSOA_H
#define PESSOA_H

#include <iostream>
#include <string>

using namespace std;

class Pessoa {
private:
        int  idade;
        string  nome;
		string  telefone;
public:
        Pessoa();
        Pessoa(string nome, int idade, string telefone);
        int  getIdade();
        void setIdade(int idade);
        string getNome();
        void setNome(string nome);
		string getTelefone();
		void setTelefone(string telefone);
};
#endif
